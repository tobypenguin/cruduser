/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TUFGaming
 */
public class InsertUser {

    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (1, 'Admin', '1234');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (2, 'Boy', 'pokemon');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (3, 'Zone', 'fgosalt');";
            stmt.executeUpdate(sql);

            conn.commit();
            stmt.close();
            conn.close();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
